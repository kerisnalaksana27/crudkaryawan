@extends('layouts.template')

@section('content')
            <div class="row">
                <div class="col-9">
                    <h2>Update Data Karyawan</h2>
                </div>
                <div class="col-3">
                    <a href="/" class="btn btn-primary mt-2 pull-right">Kembali</a>
                </div>
            <br/>
            
            @foreach($karyawan as $s)
            <form action="/update" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $s->id }}"> 
                <br/>
                <div class="mb-3">
                    <label class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama_karyawan" required="required" value="{{ $s->nama_karyawan }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">No Karyawan</label>
                    <input type="text" class="form-control" name="no_karyawan" required="required" value="{{ $s->no_karyawan }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">No Telepon</label>
                    <input type="text" class="form-control" name="no_telp_karyawan" required="required" value="{{ $s->no_telp_karyawan }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Jabatan</label>
                    <input type="text" class="form-control" name="jabatan_karyawan" required="required" value="{{ $s->jabatan_karyawan }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Divisi</label>
                    <input type="text" class="form-control" name="divisi_karyawan" required="required" value="{{ $s->divisi_karyawan }}">
                </div>
                <button type="submit" class="btn btn-primary">Update Data</button>
            </form>
            @endforeach
                <br>
            </div>
@endsection