@extends('layouts.template')

@section('content')

            @if(session('sukses'))
            <div class="alert alert-success mt-2" role="alert">
            {{ session('sukses') }}
            </div>
            @endif
            <div class="row">
                <div class="col-9">
                    <h2>Data Karyawan</h2>
                </div>
                <table class="table table-hover table-bordered border-primary table-striped">
                    <thead class="table-dark">
                        <tr>
                            <th>Nama</th>
                            <th>No Karyawan</th>
                            <th>No Telepon</th>
                            <th>Jabatan</th>
                            <th>Divisi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    @foreach($karyawan as $p)
                    <tr>
                        <td>{{ $p->nama_karyawan }}</td>
                        <td>{{ $p->no_karyawan }}</td>
                        <td>{{ $p->no_telp_karyawan }}</td>
                        <td>{{ $p->jabatan_karyawan }}</td>
                        <td>{{ $p->divisi_karyawan }}</td>
                        <td>
                            <a href="/edit/{{ $p->id }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                            |
                            <a href="/delete/{{ $p->id }}" class="btn btn-danger" onclick="return confirm('Apakah yakin ingin dihapus?')"><i class="fa fa-trash"></i> Hapus</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
                <div>
                    <br><br>
                    <a href="/add" class="btn btn-primary mt-2 pull-right">Tambah Data Karyawan</a>
                </div>
            </div>
@endsection
