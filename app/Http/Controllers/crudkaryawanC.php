<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class crudkaryawanC extends Controller
{
    public function index()
    {
        // mengambil data dari table karyawan
        $db_karyawan = DB::table('karyawan')->get();

        // mengirim data karyawan ke view index
        return view('\screen\home',['karyawan' => $db_karyawan]);
    }

    // method untuk menampilkan view form tambah data karyawan
    public function add()
    {
        // memanggil view tambah
        return view('\screen\create');
    }

    // method untuk insert data ke table karyawan
    public function store(Request $request)
    {
        // insert data ke table karyawan
        DB::table('karyawan')->insert([
            'nama_karyawan' => $request->nama_karyawan,
            'no_karyawan' => $request->no_karyawan,
            'no_telp_karyawan' => $request->no_telp_karyawan,
            'jabatan_karyawan' => $request->jabatan_karyawan,
            'divisi_karyawan' => $request->divisi_karyawan]
        );

        // alihkan halaman ke halaman karyawan
        return redirect('/')->with('sukses','Data berhasil diinput!');
    }

        // method untuk edit data karyawan
    public function edit($id)
    { 
        // mengambil data karyawan berdasarkan id yang dipilih
        $db_karyawan = DB::table('karyawan')->where('id',$id)->get(); 
        
        // passing data karyawan yang didapat ke view edit.blade.php 
        return view('\screen\edit',['karyawan' => $db_karyawan]);
    }

        // Method untuk data karyawan
    public function update(Request $request)
    {
        // update data karyawan
        DB::table('karyawan')->where('id',$request->id)->update([

            'nama_karyawan' => $request->nama_karyawan,
            'no_karyawan' => $request->no_karyawan,
            'no_telp_karyawan' => $request->no_telp_karyawan,
            'jabatan_karyawan' => $request->jabatan_karyawan,
            'divisi_karyawan' => $request->divisi_karyawan
        ]);

        // alihkan halaman ke halaman karyawan
        return redirect('/')->with('sukses','Data berhasil diupdate!');
    }

        // method untuk hapus data karyawan
    public function delete($id)
    {
        // menghapus data karyawan berdasarkan id yang dipilih
        DB::table('karyawan')->where('id',$id)->delete();
        
        // alihkan halaman ke halaman karyawan
        return redirect('/')->with('sukses','Data berhasil dihapus!');
    }
    //
}
