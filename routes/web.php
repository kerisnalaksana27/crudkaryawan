<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\crudkaryawanC;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[crudkaryawanC::class,'index']);
Route::get('/add',[crudkaryawanC::class,'add']);
Route::post('/store',[crudkaryawanC::class,'store']);
Route::get('/edit/{id}',[crudkaryawanC::class,'edit']);
Route::post('/update',[crudkaryawanC::class,'update']);
Route::get('/delete/{id}',[crudkaryawanC::class,'delete']);